# Robot Action Generator

---

## Description

Code for using LLM and AI planners to design chemistry workflows for UR3 and Kinova robotic platforms.

## Installation

1. Install Java
2. Recommended: Install poetry, and set up environment via poetry instructions
3. Or, use virtual environment and install via requirements.txt

```bash
python -m venv "env"
source env/bin/activate
pip install -r requirements.txt
```
## Entry point

`main.py`

## Output Example

(without safe paths)

```angular2html
What is your query?
i want 13 mL of acetone
NOTE: To disable printing of planning engine credits, add this line to your code: `up.shortcuts.get_environment().credits_stream = None`
*** Credits ***
* In operation mode `OneshotPlanner` at line 149 of `/home/lucy/Summer 2024/robot-action-planning/robot_utils/robot.py`, you are using the following planning engine:
* Engine name: ENHSP
* Developers:  Enrico Scala
* Description: Expressive Numeric Heuristic Search Planner.

enhsp returned:
SequentialPlan:
handler-needle-down(samplomatic)
draw-liquid(acetone, acetone, samplomatic, samplomatic base)
retract-handler-needle(samplomatic)
move(samplomatic, samplomatic base, vial tray)
handler-needle-down(samplomatic)
dispense-liquid(acetone, v1, samplomatic, vial tray)
dispense-liquid(acetone, v1, samplomatic, vial tray)
dispense-liquid(acetone, v1, samplomatic, vial tray)
dispense-liquid(acetone, v1, samplomatic, vial tray)
dispense-liquid(acetone, v1, samplomatic, vial tray)
retract-handler-needle(samplomatic)
move(samplomatic, vial tray, samplomatic base)
handler-needle-down(samplomatic)
draw-liquid(acetone, acetone, samplomatic, samplomatic base)
retract-handler-needle(samplomatic)
move(samplomatic, samplomatic base, vial tray)
handler-needle-down(samplomatic)
dispense-liquid(acetone, v1, samplomatic, vial tray)
dispense-liquid(acetone, v1, samplomatic, vial tray)
dispense-liquid(acetone, v1, samplomatic, vial tray)
dispense-liquid(acetone, v1, samplomatic, vial tray)
dispense-liquid(acetone, v1, samplomatic, vial tray)
retract-handler-needle(samplomatic)
move(samplomatic, vial tray, samplomatic base)
handler-needle-down(samplomatic)
draw-liquid(acetone, acetone, samplomatic, samplomatic base)
retract-handler-needle(samplomatic)
move(samplomatic, samplomatic base, vial tray)
handler-needle-down(samplomatic)
dispense-liquid(acetone, v1, samplomatic, vial tray)
dispense-liquid(acetone, v1, samplomatic, vial tray)
dispense-liquid(acetone, v1, samplomatic, vial tray)
retract-handler-needle(samplomatic)
move(samplomatic, vial tray, samplomatic base)
```

## Output Example

(with safe paths)
For the query: I want 7 ml of acetone, the move actions are replaced with a sequence of safe move actions.
![](safe_path_diff.png)

## Generated Python
Can be seen in `experiment.py`

### Debug Mode

To see LLM responses, set `debug` to `True`.
```json

{
  "applicationData": "{\"kinova\":{}}",
  "action": {
    "applicationData": "{\"kinova\":{\"color\":0}}",
    "handle": {
      "identifier": 10072,
      "permission": 7,
      "actionType": 7
    },
    "reachJointAngles": {
      "jointAngles": {
        "jointAngles": [
          {
            "value": 37.22854232788086,
            "jointIdentifier": 0
          },
          {
            "value": 36.947208404541016,
            "jointIdentifier": 1
          },
          {
            "value": 212.50428771972656,
            "jointIdentifier": 2
          },
          {
            "value": 218.1427459716797,
            "jointIdentifier": 3
          },
          {
            "value": 333.52630615234375,
            "jointIdentifier": 4
          },
          {
            "value": 76.96720123291016,
            "jointIdentifier": 5
          },
          {
            "value": 73.48185729980469,
            "jointIdentifier": 6
          }
        ]
      },
      "constraint": {
        "type": 0,
        "value": 0
      }
    },
    "name": "ADC_H_Safe_Joint"
  },
  "groupIdentifier": 1
},
      {
            "applicationData": "{\"kinova\":{}}",
            "action": {
              "applicationData": "{\"kinova\":{\"color\":0}}",
              "handle": {
                "identifier": 10318,
                "permission": 7,
                "actionType": 7
              },
              "reachJointAngles": {
                "jointAngles": {
                  "jointAngles": [
                    {
                      "value": 88.61054992675781,
                      "jointIdentifier": 0
                    },
                    {
                      "value": 19.632415771484375,
                      "jointIdentifier": 1
                    },
                    {
                      "value": 228.2575225830078,
                      "jointIdentifier": 2
                    },
                    {
                      "value": 224.65045166015625,
                      "jointIdentifier": 3
                    },
                    {
                      "value": 42.42793273925781,
                      "jointIdentifier": 4
                    },
                    {
                      "value": 76.35047149658203,
                      "jointIdentifier": 5
                    },
                    {
                      "value": 56.251155853271484,
                      "jointIdentifier": 6
                    }
                  ]
                },
                "constraint": {
                  "type": 0,
                  "value": 0
                }
              },
              "name": "ADC_Liquid_Handler_Home_Safe_Joint"
            },
            "groupIdentifier": 20
          },
```

## How to set up
A robotic deck for self-driving labs typically has the robotic arm, chemicals, containers and separate hardware 
equipment for performing actions like liquid/solid addition, mixing, shaking and more. Setting up the arm to move around
is in most cases a one time set up, and then for adding new hardware equipment like a liquid handler or shaker, 
new predicates, types and actions may need to be defined.

### PDDL: Planning Domain Definition Language
This is the middle language between natural language and machine-readable/executable code. Computers can read PDDL, 
but cannot directly execute hardware with PDDL. 

The domain is provided by the SDL developer, and the problem is autogenerated by the LLM and this program. In the 
domain, every action must map to a python method/function. However, not every type needs to map to a Python class; only 
the types that are used in the initial definition require a Python class.

### Python Functions
When making Python functions, the order of arguments matter. The order of arguments in a Python function/method must 
match the order of arguments defined in the domain PDDL. The first argument defined in a PDDL action will be treated 
like an object with class methods.

### Initial conditions
Defined in a JSON file.

## How to add more functionality, example: shaker
1. Define a PDDL action, and add predicates as necessary
```lisp
(:action action-name
            :parameters (?e - equipment... ?m - moveable ?l - location)
            :precondition (and  (forall (?aa - arm) (not (moveable-gripped ?g ?aa))) # the object is not gripped
                                (object-at-location ?m ?s)) # moveable object is actually at location of equipment
            :effect (predicate-stating-action-has-occured ?g)
)
```
For shaker:
```lisp
(:action shake-glassware-contents
            :parameters (?s - shakable ?g - glassware) # location and equipment have been condensed into the same type
            :precondition (and  (forall (?aa - arm) (not (moveable-gripped ?g ?aa)))
                                (object-at-location ?g ?s))
            :effect (shaken ?g)
)
```
2. Define matching Python class and method.
```python

from robot_utils.abstract_robot_classes.container import Container
class Shaker:
    def shake(self, container: Container):
        pass
```
3. Set up mapping and change necessary initial values, and the program should be able to use the Shaker and shake!

#### Notes for Later

```JSON
                "targetPose": {
                  "x": 0.12999999523162842,
                  "y": -0.4959999918937683,
                  "z": 0.3149999976158142,
                  "thetaX": -90,
                  "thetaY": -179.87013244628906,
                  "thetaZ": 180
                },

   "targetPose": {
                  "x": 0.13099999725818634,
                  "y": -0.4690000116825104,
                  "z": 0.12300000339746475,
                  "thetaX": -90.07247924804688,
                  "thetaY": -180,
                  "thetaZ": 180
                },
```
y=-0.4959999918937683-(-0.4690000116825104)=-0.02699998021 m
z=0.3149999976158142-0.12300000339746475=0.19199999421 m

put handler back in the holder after done run
everything back at home
or initial state same as final state

