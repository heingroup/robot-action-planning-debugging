from robot_utils.abstract_robot_classes.chemical import Chemical


class Liquid(Chemical):
    def __init__(self, density: float, name: str, formula: str):
        self.name = name
        self.formula = formula
        self.density: float = density


class Solid(Chemical):
    def __init__(self, density: float, name: str, formula: str):
        self.name = name
        self.formula = formula
        self.density: float = density
