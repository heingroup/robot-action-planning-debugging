from ika.thermoshaker import Thermoshaker, DummyThermoshaker

from decks.kinova.kinova_location import KinovaGridLocation
from robot_utils.abstract_robot_classes.container import Container


class Shaker(KinovaGridLocation):
    def __init__(self, name: str, sequence_location_name: str, rows: int, columns: int, spacing_x: float,
                 spacing_y: float, offset_rx: float, offset_ry: float, offset_rz: float):
        super().__init__(name, sequence_location_name, rows, columns, spacing_x, spacing_y, offset_rx, offset_ry,
                         offset_rz)
        self.shaker: DummyThermoshaker = Thermoshaker.create(port="dummy", dummy=True)

    def shake(self, container: Container, speed: int = 300):
        self.shaker.set_speed = speed
        self.shaker.start_shaking()

    def stop_shaking(self, container: Container):
        self.shaker.stop_shaking()
