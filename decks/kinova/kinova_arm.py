from __future__ import annotations

from typing import Optional, Union

from hein_robots.kinova.kinova_gen3 import KinovaGen3Arm

from decks.kinova.kinova_location import KinovaLocation, KinovaGridLocation
from decks.kinova.moving.kinova_movable_config import KinovaMoveableConfig
from robot_utils.abstract_robot_classes.arm import Arm
from robot_utils.abstract_robot_classes.container import Container
from robot_utils.abstract_robot_classes.moveable import Moveable

KINOVA_ADDRESS = "137.82.65.87"
SAFE_VELOCITY = 50


class KinovaArm(Arm):
    """
    notes and assumptions
    the arm grippers are by default open wide
    this assumption is not great, it's okay
    the only object in the z plane that might be hit is the liquid handler
    this conflict is dealt with by only moving in the x plane, and then in the y plane (or wise versa)
    """

    def __init__(self, name: str):
        super().__init__(name)
        self.arm = KinovaGen3Arm(KINOVA_ADDRESS)
        self.gripping = False
        self.arm.open_gripper(0)
        self.safe_z = 400

    def move(self, from_loc: Union[KinovaLocation, KinovaGridLocation],
             to_loc: Union[KinovaLocation, KinovaGridLocation],
             moveable: Optional[Moveable] = None):
        # ASSUMPTION: GRIPPER IS ALWAYS OPEN, UNLESS GRIPPING SOMETHING
        # this move will relocate the gripper back to the z before moving
        # TODO: the joint seems to be moving a bit
        # bRUh

        z_dif = self.arm.location.z - 400
        if abs(z_dif) > 50:
            self.arm.move_to_location(self.arm.location.translate(z=abs(z_dif)))

        x_dif = to_loc.kinova_robot_location_info.x - self.arm.location.x
        y_dif = to_loc.kinova_robot_location_info.y - self.arm.location.y

        if from_loc.name == "home" and isinstance(to_loc, KinovaLocation):
            self.arm.move_to_location(self.arm.location.translate(x=x_dif))
            self.arm.move_to_location(self.arm.location.translate(y=y_dif))

        elif to_loc.name == "home":
            # TODO: is this always safe?? need to move to z first
            self.arm.move_to_location(self.arm.location.translate(y=y_dif))
            self.arm.move_to_location(self.arm.location.translate(x=x_dif))
            self.arm.move_to_location(to_loc.kinova_robot_location_info)
        else:
            # else just move there
            if isinstance(to_loc, KinovaGridLocation) and moveable and isinstance(moveable, Container):
                grid_index_location = to_loc.grid.locations[moveable.grid_index]
                self.arm.move_to_location(grid_index_location)
            else:
                self.arm.move_to_location(to_loc.kinova_robot_location_info)

    def pick_up(self, moveable: Moveable, location: Union[KinovaLocation, KinovaGridLocation]):
        # assume arm is ready at safe approach location
        original_location = self.arm.location
        if isinstance(moveable.config, KinovaMoveableConfig) and isinstance(location, KinovaLocation):
            # opens gripper
            self.arm.open_gripper(position=moveable.config.grip_open)
            # moves to base location
            self.arm.move_to_locations(location.kinova_robot_location_info)
            # close gripper
            self.arm.open_gripper(moveable.config.grip_close)
            moveable.gripped = True
            self.gripping = True
            # move to safe z now that object is gripped
            self.arm.move_to_locations(original_location, velocity=SAFE_VELOCITY)
            # TODO: ? move back to safe approach
        else:
            raise NotImplementedError

    def put_down(self, moveable: Moveable, location: Union[KinovaLocation, KinovaGridLocation]):
        # assume arm is holding a moveable at the base, but not at the same z level
        original_location = self.arm.location
        if isinstance(moveable.config, KinovaMoveableConfig):
            # move to base
            self.arm.move_to_locations(location.kinova_robot_location_info)
            # open gripper
            self.arm.open_gripper(0)
            moveable.gripped = False
            self.gripping = False
            # move back up
            self.arm.move_to_location(original_location)
        else:
            raise NotImplementedError
