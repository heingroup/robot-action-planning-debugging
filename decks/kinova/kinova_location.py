from __future__ import annotations

import json

from hein_robots.grids import Grid
from hein_robots.robotics import Location, Cartesian

from robot_utils.abstract_robot_classes.abclocation import ABCLocation


def extract_location_from_file(sequence_location_name) -> Location:
    # TODO: update with adaptive path
    pose_file_path = f"decks/kinova/sequences/{sequence_location_name}.json"
    with open(pose_file_path) as pose_file:
        data = json.load(pose_file)
        pose = data["poses"]["pose"][0]
        target_pose = pose['reachPose']['targetPose']
        return Location(target_pose['x'], target_pose['y'], target_pose['z'],
                        target_pose['thetaX'], target_pose['thetaY'], target_pose['thetaZ']).convert_m_to_mm()


class KinovaLocation(ABCLocation):

    def __init__(self, name: str, sequence_location_name: str,
                 moveable_offset_x_y_z: tuple = (0, 0, 0)):
        # TODO need to have grids being update
        # or add to pddl???
        # since every deck in this lab has a grid, makes sense to add to pddl

        self.name = name
        self.holdable = False
        self.moveable_offset_x_y_z = moveable_offset_x_y_z
        self.kinova_robot_location_info = extract_location_from_file(sequence_location_name)


class KinovaGridLocation(ABCLocation):
    def __init__(self, name: str, sequence_location_name: str,
                 rows: int, columns: int, spacing_x: float,
                 spacing_y: float, offset_rx: float,
                 offset_ry: float, offset_rz: float,
                 moveable_offset_x_y_z: tuple = (0, 0, 0)):
        # TODO need to have grids being update
        # or add to pddl???
        # since every deck in this lab has a grid, makes sense to add to pddl

        self.name = name
        self.holdable = False
        self.moveable_offset_x_y_z = moveable_offset_x_y_z
        self.kinova_robot_location_info = extract_location_from_file(sequence_location_name)
        self.grid = Grid(location=Location(**self.kinova_robot_location_info.position.dict), rows=rows,
                         columns=columns,
                         spacing=Cartesian(x=spacing_x, y=spacing_y),
                         offset=Location(rx=offset_rx, ry=offset_ry, rz=offset_rz))
