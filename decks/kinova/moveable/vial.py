from __future__ import annotations

from typing import Union, Optional

from decks.kinova.chemicals import Liquid, Solid
from decks.kinova.moving.kinova_movable_config import KinovaMoveableConfig
from robot_utils.abstract_robot_classes.chemical import Chemical
from robot_utils.abstract_robot_classes.container import Container


class Vial(Container):
    def __init__(self, name: str, config: Optional[KinovaMoveableConfig] = None, grid_index: str = "A1"):
        super().__init__(name=name, grid_index=grid_index, config=config)
        self.chemical: Union[Liquid, Solid] = None
        self.amount_ul: int = 0

    def update_contents(self, chemical: Chemical, amount_ul: int):
        self.chemical = chemical
        self.amount_ul += amount_ul
