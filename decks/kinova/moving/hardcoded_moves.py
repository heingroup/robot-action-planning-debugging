from hein_robots.kinova.kinova_gen3 import KinovaGen3Arm

from decks.kinova.kinova_location import KinovaLocation

VIAL_TRAY = 'ADC_VialTray_1_1'
CAPPED_VIAL_TRAY = 'ADC_Vial_Tray_with_capped_vial_1_1'
SAFE_H_VIAL_TRAY = "ADC_Safe_Joint_Vial_tray"
SHAKER_TRAY = "ADC_Shaker_1_1"
FILTRATION_TRAY = "ADC_Filtration_Vial_1"
LIQUID_FILTRATION_TRAY = "ADC_Liquid_Filtration_Left_1"
LIQUID_SHAKER_TRAY = "ADC_Liquid_Shaker_1_4"
LIQUID_BIG_REACTANTS = "ADC_Liquid_Big_Reactant_1"
LIQUID_SMALL_REACTANTS = "ADC_Liquid_Small_Reactant_1_1"
SAFE_LIQUID_APPROACH = "ADC_Liquid_Approach_H_Safe_Joint"
SAFE_LIQUID_HOME = "ADC_Liquid_Handler_Home_Safe_Joint"
SAFE_LIQUID_FILTRATION = "ADC_Liquid_Filtration_H_Joint_Safe"
SAFE_LIQUID_MIXED = "ADC_Liquid_Mixed_H_Joint_Safe"
SAFE_LIQUID_REACTANT = "ADC_Liquid_Reactant_H_Joint_Safe"
SAFE_H_FILTRATION = "ADC_Filtration_H_Joint_safe"
SAFE_V_FILTRATION = "ADC_Filtration_V_Joint_safe"
SAFE_H_JOINT = "ADC_H_Safe_Joint"
SAFE_H = "ADC_H_safe"
SAFE_VELOCITY = 50
SUPER_SAFE_VELOCITY = 10
LIFT_ROOM = 40  # mm
SAFE_HEIGHT_LIFT = 200  # mm

GRIPPER_OPEN_DEFAULT = 0.62
GRIPPER_CLOSE_DEFAULT = 0.88
GRIPPER_CLOSE_SM = 0.7
GRIPPER_OPEN_SM = 0.5
GRIPPER_CLOSE_HPLC_VIAL = 1
GRIPPER_CLOSE_CAPLESS_VIAL = 0.95
GRIPPER_OPEN_HPLC_VIAL = 0.85
GRIPPER_CLOSE_FILTER = 0.69
GRIPPER_OPEN_FILTER = 0.42
GRIPPER_CLOSE_LIQUID = 0.29
GRIPPER_OPEN_LIQUID = 0
SAFE_GRIPPER_VELOCITY = 0.05
FAST_GRIPPER_VELOCITY = 1


def home_to_solvent_tray(arm: KinovaGen3Arm, to_location: KinovaLocation, index: str = "A1"):
    arm.move_to_locations(to_location.kinova_robot_location_info, velocity=SAFE_VELOCITY)


def solvent_tray_to_home(arm: KinovaGen3Arm, to_location: KinovaLocation, index: str = "A1"):
    arm.move_to_locations(to_location.kinova_robot_location_info, velocity=SAFE_VELOCITY)


def home_to_liquid_handler_base(arm: KinovaGen3Arm, to_location: KinovaLocation):
    arm.move_to_locations(to_location.kinova_robot_location_info, velocity=SAFE_VELOCITY)


def liquid_handler_base_to_home(arm: KinovaGen3Arm, to_location: KinovaLocation):
    arm.move_to_locations(to_location.kinova_robot_location_info, velocity=SAFE_VELOCITY)


def home_to_vial_tray(arm: KinovaGen3Arm, to_location: KinovaLocation, index: str = "A1"):
    arm.move_to_locations(to_location.kinova_robot_location_info, velocity=SAFE_VELOCITY)


def vial_tray_to_home(arm: KinovaGen3Arm, to_location: KinovaLocation, index: str = "A1"):
    arm.move_to_locations(to_location.kinova_robot_location_info, velocity=SAFE_VELOCITY)
