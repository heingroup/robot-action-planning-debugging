import time

from hein_robots.kinova.kinova_gen3 import KinovaGen3Arm
from hein_robots.robotics import Location

from decks.kinova.moving.hardcoded_moves import SAFE_H_JOINT, SAFE_LIQUID_APPROACH, SAFE_VELOCITY, SUPER_SAFE_VELOCITY, GRIPPER_OPEN_HPLC_VIAL, GRIPPER_CLOSE_CAPLESS_VIAL, \
    SAFE_GRIPPER_VELOCITY


def liquid_handler_pick_up(arm: KinovaGen3Arm, sequences, location, grip_open: float, grip_closed: float):
    base_location = sequences.locations["ADC_Liquid_Handler_Home"]

    arm.open_gripper(grip_open)
    arm.move_to_locations(base_location.translate(y=80))
    arm.move_to_locations(base_location, velocity=SAFE_VELOCITY)
    arm.open_gripper(grip_closed)
    time.sleep(1)
    arm.open_gripper(grip_closed)
    arm.move_to_locations(base_location.translate(z=30), velocity=SUPER_SAFE_VELOCITY)
    arm.move_to_locations(base_location.translate(z=80), velocity=SAFE_VELOCITY)
    arm.move_to_locations(base_location.translate(z=80, y=120), velocity=SAFE_VELOCITY)


def liquid_handler_put_down(arm: KinovaGen3Arm, sequences, location, grip_open: float, grip_closed: float):
    safe_liquid_approach = sequences.joints[SAFE_LIQUID_APPROACH]
    base_location = sequences.locations["ADC_Liquid_Handler_Home"]

    arm.move_to_locations(base_location.translate(z=80, y=120), velocity=SAFE_VELOCITY)
    arm.move_to_locations(base_location.translate(z=80), velocity=SAFE_VELOCITY)
    arm.move_to_locations(base_location.translate(z=30), velocity=SUPER_SAFE_VELOCITY)
    arm.move_to_locations(base_location, velocity=SUPER_SAFE_VELOCITY)

    arm.open_gripper(grip_open)
    time.sleep(1)
    arm.open_gripper(grip_open)
    time.sleep(1)

    arm.move_to_locations(base_location.translate(y=80), velocity=SAFE_VELOCITY)
    arm.move_joints(safe_liquid_approach)


def vial_pick_up(arm: KinovaGen3Arm, sequences, holdable: Location, grip_open: float, grip_closed: float):
    arm.move_to_locations(holdable.translate(z=80))
    arm.open_gripper(GRIPPER_OPEN_HPLC_VIAL)
    arm.move_to_locations(holdable.translate(z=20))
    arm.move_to_locations(holdable, velocity=SAFE_VELOCITY)
    arm.open_gripper(GRIPPER_CLOSE_CAPLESS_VIAL, velocity=SAFE_GRIPPER_VELOCITY)
    arm.move_to_locations(holdable.translate(z=80), velocity=SAFE_VELOCITY)


def vial_put_down(arm: KinovaGen3Arm, sequences, grip_open: float, grip_closed: float):
    safe_location = sequences.joints[SAFE_H_JOINT]
    base_location = sequences.locations["ADC_capless_balance"]
    arm.move_joints(safe_location)
    arm.move_to_locations(base_location.translate(z=80))
    arm.move_to_locations(base_location.translate(z=20))
    arm.move_to_locations(base_location, velocity=SUPER_SAFE_VELOCITY)
    arm.open_gripper(GRIPPER_OPEN_HPLC_VIAL)
    arm.move_to_locations(base_location.translate(z=80))
    arm.move_joints(safe_location)
