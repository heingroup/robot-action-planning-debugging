from dataclasses import dataclass
from typing import Callable, Optional

from robot_utils.abstract_robot_classes.moveableconfig import MoveableConfig, MoveableSequence


@dataclass
class KinovaMoveableSequence(MoveableSequence):
    location_A: str
    location_B: str
    sequence_from_A_to_B: Optional[Callable] = None
    sequence_from_B_to_A: Optional[Callable] = None


@dataclass
class KinovaMoveableConfig(MoveableConfig):
    name: str
    safe_moves: list[KinovaMoveableSequence]
    grip_open: float
    grip_close: float
