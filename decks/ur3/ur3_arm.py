from robot_utils.abstract_robot_classes.abclocation import ABCLocation
from robot_utils.abstract_robot_classes.arm import Arm
from robot_utils.abstract_robot_classes.moveable import Moveable


class UR3Arm(Arm):
    def move(self, moveable: Moveable, from_loc: ABCLocation, to_loc: ABCLocation):
        pass

    def pick_up(self, moveable: Moveable, location: ABCLocation):
        pass

    def put_down(self, moveable: Moveable, location: ABCLocation):
        pass

    def __init__(self, name: str):
        super().__init__(name)
