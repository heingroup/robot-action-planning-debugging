from decks.kinova.moving.kinova_movable_config import *
from decks.kinova.kinova_arm import *
from decks.kinova.moveable.europa import *
from decks.kinova.moveable.vial import *
from decks.kinova.moving.kinova_movable_config import *

home = KinovaLocation(name='home', sequence_location_name='central_home')
kinova = KinovaArm(name='kinova')
europa_base = KinovaLocation(name='europa base', sequence_location_name='liquid_base_xy')
solvent_tray = KinovaGridLocation(name='solvent tray', sequence_location_name='solvent_tray_draw', rows=1, columns=2,
                                  spacing_x=38, spacing_y=0, offset_rx=-90, offset_ry=-180, offset_rz=180)
vial_tray = KinovaGridLocation(name='vial tray', sequence_location_name='vial_tray_dispense', rows=6, columns=4,
                               spacing_x=20, spacing_y=20, offset_rx=-90, offset_ry=180, offset_rz=180)
europa = EuropaHandling(name='europa', config=KinovaMoveableConfig(name='europa', safe_moves=[
    KinovaMoveableSequence(location_A='home', location_B='vial tray'),
    KinovaMoveableSequence(location_A='home', location_B='europa base'),
    KinovaMoveableSequence(location_A='home', location_B='solvent tray')], grip_open=0, grip_close=0.29),
                        max_volume_ul=1000)
vial_1 = Vial(name='vial 1', config=KinovaMoveableConfig(name='vial 1', safe_moves=[
    KinovaMoveableSequence(location_A='home', location_B='vial tray'),
    KinovaMoveableSequence(location_A='home', location_B='kinova shaker')], grip_open=0, grip_close=0.5),
              grid_index='A1')
vial_2 = Vial(name='vial 2', config=KinovaMoveableConfig(name='vial 2', safe_moves=[
    KinovaMoveableSequence(location_A='home', location_B='vial tray'),
    KinovaMoveableSequence(location_A='home', location_B='kinova shaker')], grip_open=0, grip_close=0.5),
              grid_index='A2')
acetone = Liquid(name='acetone', density=0, formula='C3H6O')
salt = Solid(name='salt', density=2, formula='NaCl')

kinova.arm.move_to_location(home.kinova_robot_location_info)
# ADD ^^

kinova.move(home, europa_base)
kinova.pick_up(europa, europa_base)
kinova.move(europa_base, home, europa)
kinova.move(home, solvent_tray, europa)
# europa.needle_down()
# europa.draw_liquid(acetone, solvent_tray)
# europa.needle_up()
kinova.move(solvent_tray, home, europa)
kinova.move(home, vial_tray, europa)
# europa.needle_down()
europa.dispense(acetone, vial_2, vial_tray, kinova)
# europa.needle_up()
kinova.move(vial_tray, home, europa)
kinova.move(home, solvent_tray, europa)
# europa.needle_down()
# europa.draw_liquid(acetone, solvent_tray)
# europa.needle_up()
kinova.move(solvent_tray, home, europa)
kinova.move(home, vial_tray, europa)
# europa.needle_down()
europa.dispense(acetone, vial_1, vial_tray, kinova)
# europa.needle_up()
kinova.move(vial_tray, home, europa)
kinova.move(home, europa_base, europa)
kinova.put_down(europa, europa_base)
kinova.move(europa_base, home)

kinova.move(europa_base, home)
# ADD  ^^^
