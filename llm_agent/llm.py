from typing import Callable

import marshmallow_dataclass
import ollama
from pydantic import TypeAdapter
from unified_planning.model import Fluent, Problem

from llm_agent.llm_message import LLMMessage
from robot_utils.dataclasses.pddl_predicates import LLMPredicates, ImpliedInitialStatePredicateStr
from robot_utils.dataclasses.pddl_dataclasses import *
from robot_utils.dataclasses.pddl_validation import check_type_and_father


# TODO: make the llm return everything lowercase

class LLMAgent:
    def __init__(self, model: str, critic: str, ip: str, problem: Problem):
        self.debug = True
        self.restart_process = False
        self.client = ollama.Client(host=ip)
        self.model: str = model
        self.critic: str = critic
        self.incomplete_problem: Problem = problem
        self.llm_derived_problem_schema = marshmallow_dataclass.class_schema(PDDLProblemInstance)()

    def send_critic_message(self, message) -> str:
        return self.client.chat(model=self.model,
                                messages=[
                                    {
                                        'role': 'user',
                                        'content': message
                                    }])['message']['content']

    def send_llm_message(self, message: str) -> str:
        return self.client.chat(model=self.model,
                                messages=[
                                    {
                                        'role': 'user',
                                        'content': message
                                    }])['message']['content']

    def llm_feedback_loop(self, params: LLMMessage, checks: [Callable] = []):
        while True:
            query = params.query + (f"Here was your original answer: "
                                    f"{params.llm_response_str}, use this feedback: "
                                    f"{params.feedback} to correct your answer. "
                                    f"Only return the JSON.") if len(
                params.feedback) != 0 else params.query
            params.llm_response_str = self.send_llm_message(query).lower()
            parse_passed = params.try_to_parse()
            serialize_passed = params.try_to_serialize()
            checks_passed = []
            if serialize_passed:
                for check in checks:
                    checks_passed.append(check(params.llm_return_object))

            if (parse_passed and serialize_passed) and not self.debug and all(checks_passed):
                break

            if serialize_passed and parse_passed and all(checks_passed):
                approve = input(f"{params.llm_return_object}\nDoes this make sense?")
                if "y" in str(approve).lower():
                    break
                else:
                    params.feedback.append(input("What should be changed?"))
            else:
                print(f"The LLM failed to fulfill these checks: {params.feedback[-1]}")

    def get_grounded_predicates(self, prompt: str, objects: ChosenObjects) -> LLMPredicates:
        object_types = [o.kind for o in objects.objects]

        # check types
        def object_types_match(f: Fluent):
            return all(map(lambda p: p.type.name in object_types, f.signature))

        def format_predicate(f: Fluent):
            name = f.name
            signature = [f"{p.name}: {p.type}" for p in f.signature]
            return_type = f.type
            return f"{name}({', '.join(signature)}) -> {return_type}\n"

        predicates_with_matching_object_types = filter(object_types_match, self.incomplete_problem.fluents)
        predicate_signature_formatted = list(map(format_predicate, predicates_with_matching_object_types))

        object_json = {o.name: o.kind for o in objects.objects}

        if len(predicate_signature_formatted) == 0:
            raise Exception("No valid predicates found, either no plan can be made from existing predicates or "
                            "the wrong objects were chosen")

        predicate_llm_message = LLMMessage(
            query=f"""Here are the predicates you can use: {"".join(predicate_signature_formatted)}, however only
            use the predicates necessary to complete this prompt: {prompt}.
            Here are the objects and their types, that you can use: {object_json}. 
            Return your answer in a JSON, following this JSON schema.
            You do not have anything prepared, and must use the predicates to prepare 
            all everything the prompt asks for.
            Additionally, the predicate signatures must match the object types.
            
            Follow this schema:
             
            {TypeAdapter(LLMPredicates).json_schema()}
             
             Each predicate requires a final value, based on the return type of the predicate.
             For the objects, just refer to their string name.
             
            Only return the JSON.
            """, user_dataclass=LLMPredicates)

        # check objects exist and object type
        def check_object_existence_type(llm_predicates: LLMPredicates) -> bool:
            object_names = [o.name for o in objects.objects]
            predicates: list[ImpliedInitialStatePredicateStr] = llm_predicates.predicates
            for implied_initial_predicate in predicates:
                predicate_name = implied_initial_predicate.predicate
                predicate_objects: list[str] = implied_initial_predicate.objects
                for predicate_object in predicate_objects:
                    if predicate_object not in object_names:
                        predicate_llm_message.feedback.append(f"{predicate_object} object does not exist.")
                        return False
                    pddl_object_type = self.incomplete_problem.object(predicate_object).type
                    pddl_predicate_signature_types = [p.type for p in
                                                      self.incomplete_problem.fluent(predicate_name).signature]
                    if not any(map(lambda sig_type: check_type_and_father(pddl_object_type, sig_type.name),
                                   pddl_predicate_signature_types)):
                        predicate_llm_message.feedback.append(
                            f"{predicate_object} is of type {pddl_object_type.name}, which "
                            f"is not a valid type conforming to the {predicate_name} "
                            f"signature.")
                        return False
            return True

        # need to add some checks, more context, parse with the PDDLReader
        # need to add to growing objects

        self.llm_feedback_loop(predicate_llm_message, checks=[check_object_existence_type])

        return predicate_llm_message.llm_return_object

    def get_pddl_objects(self, prompt: str) -> ChosenObjects:
        pickable_objects = list(filter(lambda o: "able" not in o.type.name and "equip" not in o.type.name,
                                       self.incomplete_problem.all_objects))

        subgoal_llm_params = LLMMessage(query=f""" 
                    Gather the objects you would need to complete this prompt: {prompt},
                    {[("name: " + o.name, "kind: " + o.type.name) for o in pickable_objects]} 
                    Return your answer in a JSON, following this JSON schema.
                     
                     {TypeAdapter(ChosenObjects).json_schema()}
                     
                     Make sure these objects can complete this prompt step: {prompt}
                     
                     Only return the JSON, and use a minimal number of objects.
                     """, user_dataclass=ChosenObjects)

        self.llm_feedback_loop(subgoal_llm_params)

        return subgoal_llm_params.llm_return_object

    def prose_to_pddl(self, debug: bool = True) -> list[ImpliedInitialStatePredicate]:
        self.debug = debug
        query = input("What is your query?\n")
        objects: ChosenObjects = self.get_pddl_objects(query)
        return [ImpliedInitialStatePredicate(predicate=p.predicate, objects=p.objects, final_value=p.final_value)
                for p in self.get_grounded_predicates(query, objects).predicates]
