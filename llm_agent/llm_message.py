from __future__ import annotations

import json
from typing import Mapping, Any, Dict, Union

import marshmallow_dataclass
from pydantic.dataclasses import dataclass
from unified_planning.io import PDDLReader


def extract_largest_json(llm_res, left, right) -> Dict:
    llm_res_remove_newlines = "".join(llm_res.splitlines())
    left_index = float("inf")
    right_index = -float("inf")
    string_len = len(llm_res_remove_newlines)
    for n in range(string_len):
        left_char = llm_res_remove_newlines[n]
        right_char = llm_res_remove_newlines[string_len - n - 1]
        if left_char == left:
            left_index = n if n < left_index else left_index
        if right_char == right:
            right_index = (string_len - n - 1) if right_index < (string_len - n - 1) else right_index

    return json.loads(llm_res_remove_newlines[left_index: right_index + 1])


class LLMMessage:
    def __init__(self, query: str, user_dataclass: dataclass) -> None:
        self.dataclass = marshmallow_dataclass.class_schema(user_dataclass)()
        self.llm_return_object: user_dataclass = None
        self.query: str = query
        self.llm_response: Mapping[str, Any] = {}
        self.llm_response_str: str = ""
        self.feedback = []

    def try_to_parse(self):
        try:
            self.llm_response = extract_largest_json(self.llm_response_str, "{", "}")
            return True
        except Exception as e:
            self.feedback.append(str(e))
            return False

    def try_to_serialize(self) -> bool:
        try:
            self.llm_return_object = self.dataclass.load(self.llm_response)
            return True
        except Exception as e:
            self.feedback.append(str(e))
            return False
