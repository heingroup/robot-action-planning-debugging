from decks.kinova.chemicals import Solid, Liquid
from decks.kinova.components.shaker import Shaker
from decks.kinova.kinova_arm import *
from decks.kinova.moveable.europa import EuropaHandling
from decks.kinova.moveable.vial import Vial
from decks.kinova.moving.kinova_movable_config import KinovaMoveableConfig, KinovaMoveableSequence
from robot_utils.robot_planner import RobotActionGenerator

PERSONAL_IP = "127.0.0.1:11434"
HEIN_IP = "137.82.65.246"
CRITIC = "mistral"
GEMMA = "gemma"
MODEL = "llama3"
DOMAIN_SIMPLE = r"pddl/simple-liquid-domain.pddl"
INITIAL = r"decks/kinova/initial.json"

if __name__ == "__main__":
    robot = RobotActionGenerator(debug=False,
                                 central_node="home",
                                 domain_path=DOMAIN_SIMPLE,
                                 user_input=INITIAL,
                                 model=MODEL,
                                 critic=CRITIC,
                                 ip=HEIN_IP,
                                 action_mapping={"pickup": Arm.pick_up,
                                                 "putdown": Arm.put_down,
                                                 "move": Arm.move,
                                                 "free-move": Arm.move,
                                                 "draw-liquid": EuropaHandling.draw_liquid,
                                                 "dispense-liquid": EuropaHandling.dispense,
                                                 "retract-handler-needle": EuropaHandling.needle_up,
                                                 "handler-needle-down": EuropaHandling.needle_down,
                                                 "shake-glassware-contents": Shaker.shake},
                                 object_mapping={"liquid": Liquid,
                                                 "solid": Solid,
                                                 "location": KinovaLocation,
                                                 "glassware": Vial,
                                                 "shakable": Shaker,
                                                 "holdable": KinovaGridLocation,
                                                 "handlerequip": EuropaHandling,
                                                 "arm": KinovaArm},
                                 moveable_configs=[
                                     KinovaMoveableConfig(name="europa",
                                                          safe_moves=[
                                                              KinovaMoveableSequence(
                                                                  location_A="home",
                                                                  location_B="vial tray",
                                                              ),
                                                              KinovaMoveableSequence(
                                                                  location_A="home",
                                                                  location_B="europa base",
                                                              ),
                                                              KinovaMoveableSequence(
                                                                  location_A="home",
                                                                  location_B="solvent tray",
                                                              )
                                                          ],
                                                          grip_open=0,
                                                          grip_close=0.29),
                                     KinovaMoveableConfig(name="vial 1",
                                                          safe_moves=[
                                                              KinovaMoveableSequence(
                                                                  location_A="home",
                                                                  location_B="vial tray",
                                                              ),
                                                              KinovaMoveableSequence(
                                                                  location_A="home",
                                                                  location_B="kinova shaker",
                                                              )
                                                          ],
                                                          grip_open=0,
                                                          grip_close=0.50),
                                     KinovaMoveableConfig(name="vial 2",
                                                          safe_moves=[
                                                              KinovaMoveableSequence(
                                                                  location_A="home",
                                                                  location_B="vial tray",
                                                              ),
                                                              KinovaMoveableSequence(
                                                                  location_A="home",
                                                                  location_B="kinova shaker",
                                                              )
                                                          ],
                                                          grip_open=0,
                                                          grip_close=0.50)],
                                 moveable_sequence_type=KinovaMoveableSequence,
                                 moveable_config_type=KinovaMoveableConfig)
