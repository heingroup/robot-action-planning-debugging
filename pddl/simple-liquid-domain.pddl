(define (domain weigh)

(:requirements :strips :typing :numeric-fluents)

(:types         chemical moveable location arm - object
                handlerable holdable - location
                shakable addable - holdable
                glassware handlerequip - moveable
                liquid solid - chemical)

(:predicates
            (object-allowed-at-location ?m - moveable ?l - location)
            (object-at-location ?m - moveable ?l - location)
            (moveable-home ?m - moveable ?l - location)
            (handler-chemical ?c - chemical ?h - handlerequip)
            (safe-to-move ?m - moveable)
            (needle-down ?h - handlerequip)
            (moveable-gripped ?m - moveable ?a - arm)
            (fillable-location ?l - location)
            (arm-location ?a - arm ?l - location)
            (shaken ?g - glassware)
)

(:functions
            (solid-added-to-glassware ?s - solid ?g - glassware)
            (liquid-added-to-glassware ?l - liquid ?g - glassware)
            (handler-amount ?c - chemical ?h - handlerequip)
            (max-volume ?h - handlerequip)
)

(:action pickup
        :parameters (?a - arm ?m - moveable ?l - location)
        :precondition (and (arm-location ?a ?l)
                            (object-at-location ?m ?l)
                            (safe-to-move ?m)
                            (forall (?mm - moveable) (not (moveable-gripped ?mm ?a)))
                            (forall (?aa - arm) (not (moveable-gripped ?m ?aa))))
        :effect (moveable-gripped ?m ?a)
)

(:action putdown
        :parameters (?a - arm ?m - moveable ?l - location)
        :precondition (and (arm-location ?a ?l)
                            (moveable-gripped ?m ?a)
                           (object-allowed-at-location ?m ?l))
        :effect (not (moveable-gripped ?m ?a))
)

(:action free-move
        :parameters (?a - arm ?from - location ?to - location)
        :precondition (and (arm-location ?a ?from) (forall (?mm - moveable) (not (moveable-gripped ?mm ?a))))
        :effect (and  (arm-location ?a ?to)
                    (not (arm-location ?a ?from)))
)

(:action move
        :parameters (?a - arm ?from - location ?to - location ?m - moveable)
        :precondition (and  (moveable-gripped ?m ?a)
                            (safe-to-move ?m)
                            (object-at-location ?m ?from))
        :effect (and    (object-at-location ?m ?to)
                    (not (object-at-location ?m ?from))
                    (arm-location ?a ?to)
                    (not (arm-location ?a ?from)))
)

(:action draw-liquid
            :parameters (?h - handlerequip ?l - liquid ?loc - location)
            :precondition (and  (needle-down ?h)
                                (fillable-location ?loc)
                                (object-at-location ?h ?loc)
                                (= (handler-amount ?l ?h) 0)
                          )
            :effect (and
                        (assign (handler-amount ?l ?h) (max-volume ?h))
                        (handler-chemical ?l ?h)
                        )
)

(:action dispense-liquid
            :parameters (?h - handlerequip ?l - liquid ?g - glassware ?loc - addable ?a - arm)
            :precondition (and
                            (moveable-gripped ?h ?a)
                            (needle-down ?h)
                            (handler-chemical ?l ?h)
                            (> (handler-amount ?l ?h) 0)
                            (object-at-location ?h ?loc)
                            (object-at-location ?g ?loc))
            :effect (and
                    (increase (liquid-added-to-glassware ?l ?g) (max-volume ?h))
                    (decrease (handler-amount ?l ?h) (max-volume ?h))))

(:action retract-handler-needle
            :parameters (?h - handlerequip)
            :precondition (and (not (safe-to-move ?h)) (needle-down ?h))
            :effect (and (not (needle-down ?h)) (safe-to-move ?h))
)

(:action handler-needle-down
            :parameters (?h - handlerequip)
            :precondition (not (needle-down ?h))
            :effect (and (needle-down ?h) (not (safe-to-move ?h)))
)

(:action shake-glassware-contents
            :parameters (?s - shakable ?g - glassware )
            :precondition (and  (forall (?aa - arm) (not (moveable-gripped ?g ?aa)))
                                (object-at-location ?g ?s))
            :effect (shaken ?g)
)
)
