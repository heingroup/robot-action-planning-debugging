from abc import ABC, abstractmethod
from typing import Optional

from decks.kinova.moving.kinova_movable_config import KinovaMoveableConfig
from robot_utils.abstract_robot_classes.chemical import Chemical
from robot_utils.abstract_robot_classes.moveable import Moveable


class Container(Moveable, ABC):
    def __init__(self, grid_index: str, name: str, config: Optional[KinovaMoveableConfig] = None):
        super().__init__(name, config)
        self.grid_index = grid_index

    @abstractmethod
    def update_contents(self, chemical: Chemical, amount_ul: int):
        pass
