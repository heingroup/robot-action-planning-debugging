from abc import ABC, abstractmethod

from robot_utils.abstract_robot_classes.arm import Arm
from robot_utils.abstract_robot_classes.chemical import Chemical
from robot_utils.abstract_robot_classes.container import Container
from robot_utils.abstract_robot_classes.abclocation import ABCLocation
from robot_utils.abstract_robot_classes.moveable import Moveable


class Handler(Moveable, ABC):
    @abstractmethod
    def dispense(self, chemical: Chemical, container: Container, location: ABCLocation, arm: Arm):
        pass
