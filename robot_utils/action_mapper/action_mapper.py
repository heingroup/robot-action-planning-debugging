import ast
import inspect
import textwrap
from _ast import ImportFrom, alias, Module, Constant, Name, Assign, Expr, Call, Store, Load, keyword, Attribute
from typing import Callable, Union, Optional

from unified_planning.model import Type, Problem
from unified_planning.plans import ActionInstance

from robot_utils.abstract_robot_classes.moveableconfig import MoveableConfig
from robot_utils.action_mapper.utils import extract_class_def_params, generate_config_keywords
from robot_utils.dataclasses.pddl_objects import Object, LocationObject


class ActionMapper:
    def __init__(self,
                 problem: Problem,
                 pddl_objects: list[Union[Object, LocationObject]],
                 object_mapping: dict[str, any],
                 action_mapping: dict[str, Callable],
                 plan: list[ActionInstance],
                 debug: bool,
                 moveable_configs: list[MoveableConfig],
                 moveable_sequence_type: type,
                 moveable_config_type: type):
        self.moveable_config_type = moveable_config_type
        self.moveable_sequence_type = moveable_sequence_type
        self.action_config: dict[str, Callable] = action_mapping
        self.moveable_configs: list[MoveableConfig] = moveable_configs
        self.debug = debug
        self.problem: Problem = problem
        self.pddl_objects: list[Union[Object, LocationObject]] = pddl_objects
        self.object_space: dict[str, any] = object_mapping
        self.plan = plan
        self.mapped_objects: dict[str, Assign] = self.create_objects()
        self.mapped_actions: list[Expr] = self.create_actions()

    def create_objects(self) -> dict[str, Assign]:
        mapped_objects: dict[str, Assign] = {}

        def find_python_object_class(object_type: str) -> str:
            lowest_father_type = object_type
            pddl_type: Type = self.problem.user_type(lowest_father_type)

            while pddl_type:
                try:
                    class_name, class_args = extract_class_def_params(self.object_space[lowest_father_type])
                    return class_name
                except:
                    pddl_type: Type = self.problem.user_type(lowest_father_type)
                    if pddl_type.father:
                        lowest_father_type = pddl_type.father.name

        def form_config_ast(moveable_config: Optional[MoveableConfig]) -> Optional[keyword]:
            if not moveable_config:
                return None

            config_class_name, config_arguments = extract_class_def_params(self.moveable_config_type)
            sequence_class_name, sequence_arguments = extract_class_def_params(self.moveable_sequence_type)

            return keyword(arg="config",
                           value=Call(args=[], func=Name(id=config_class_name), keywords=generate_config_keywords(
                               config_arguments, sequence_class_name, sequence_arguments, moveable_config)))

        for pddl_object in self.pddl_objects:
            snake_case_name = pddl_object.name.replace(" ", "_")
            object_moveable_config: Optional[MoveableConfig] = next(
                filter(lambda m: m.name == pddl_object.name, self.moveable_configs), None)

            moveable_ast = form_config_ast(object_moveable_config)

            keywords = [keyword(arg="name", value=Constant(pddl_object.name))]
            if moveable_ast:
                keywords.append(moveable_ast)
            if pddl_object.args:
                keywords += [keyword(arg=k, value=Constant(v)) for k, v in pddl_object.args.items()]

            mapped_objects[snake_case_name] = Assign(lineno=0,
                                                     targets=[Name(id=snake_case_name, ctx=Store())],
                                                     value=Call(args=[], func=Name(
                                                         id=f"{find_python_object_class(pddl_object.kind)}",
                                                         ctx=Load()),
                                                                keywords=keywords))

        return mapped_objects

    def create_actions(self) -> list[Expr]:

        python_actions = []
        for action_instance in self.plan:
            def create_action(function: Callable) -> Expr:
                # TODO: figure out classes
                function_ast = ast.parse(textwrap.dedent(inspect.getsource(function)))
                function_def = function_ast.body[0]
                function_name = function_def.name
                class_name = action_instance.actual_parameters[0].object().name.replace(" ", "_")
                args = [Name(id=p.object().name.replace(" ", "_"), ctx=Load()) for p in
                        action_instance.actual_parameters[1:]]

                # TODO classes? functions? top-level??
                # need to think about how the generated file should look
                # TODO: check args
                return Expr(value=Call(
                    args=args,
                    func=Attribute(attr=function_name, ctx=Load(), value=Name(id=class_name, ctx=Load())),
                    keywords=[]))

            pddl_name = action_instance.action.name
            python_actions.append(create_action(self.action_config[pddl_name]))

        return python_actions

    def generate_file(self) -> str:
        def make_import(o) -> ImportFrom:
            module: str = inspect.getmodule(o).__file__
            # TODO: generalize
            root_lvl = "robot-action-planning"
            # TODO: i don't know if this works on windows
            import_path = module.split(root_lvl, 1)[1].split('.py')[0].replace("/", ".")[1:]

            return ImportFrom(module=import_path, names=[alias(name='*')], level=0)

        objs_to_import = [self.moveable_config_type, self.moveable_sequence_type] + list(self.object_space.values())
        imports: list[ImportFrom] = [make_import(o) for o in objs_to_import]

        return ast.unparse(
            Module(body=imports + list(self.mapped_objects.values()) + self.mapped_actions, type_ignores=[]))
