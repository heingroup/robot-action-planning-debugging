import ast
import inspect
import textwrap
from _ast import ClassDef, Constant, Name, keyword, List, Call
from collections import namedtuple
from typing import Callable

from robot_utils.abstract_robot_classes.moveableconfig import MoveableSequence

argument = namedtuple('arguments', ['type_annotation', 'id'])


def extract_class_def_params(obj_class) -> (str, list[argument]):
    module = ast.parse(textwrap.dedent(inspect.getsource(obj_class)))
    class_def: ClassDef = module.body[0]

    # need the annotation and target.id
    ann_assigns: list[argument] = []
    for ann_assign in class_def.body:
        type_annotation = ""
        try:
            type_annotation = ann_assign.annotation.id
        except AttributeError:
            ann_assigns.append(argument("list", "safe_moves"))
            continue
        id = ann_assign.target.id
        ann_assigns.append(argument(type_annotation, id))

    class_name: str = class_def.name
    return class_name, ann_assigns


def get_func_name(function: Callable) -> str:
    function_ast = ast.parse(textwrap.dedent(inspect.getsource(function)))
    function_def = function_ast.body[0]
    function_name = function_def.name
    return function_name


def generate_sequence_keywords(sequence_arguments, sequence: MoveableSequence) -> list[keyword]:
    seq_keywords = []
    for arg in sequence_arguments:
        arg_id = arg.id
        try:
            value = Constant(getattr(sequence, arg.id)) if arg.type_annotation != "Callable" else (
                Name(id=get_func_name(getattr(sequence, arg.id))))
            seq_keywords.append(keyword(arg=arg_id, value=value))
        except AttributeError:
            continue
    return seq_keywords


def parse_moveable_sequence(sequence_class_name, sequence_arguments, sequences: list[MoveableSequence]) -> List:
    return List(elts=[Call(args=[],
                           func=Name(id=sequence_class_name),
                           keywords=generate_sequence_keywords(sequence_arguments, sequence)) for sequence in
                      sequences])


def generate_config_keywords(config_arguments, sequence_class_name, sequence_arguments, moveable_config):
    config_keywords = []
    for arg in config_arguments:
        arg_id = arg.id
        value = parse_moveable_sequence(sequence_class_name, sequence_arguments,
                                        moveable_config.safe_moves) if arg_id == "safe_moves" else Constant(
            getattr(moveable_config, arg.id)) if arg.type_annotation != "Callable" else Name(
            id=get_func_name(getattr(moveable_config, arg.id)))
        config_keywords.append(keyword(arg=arg_id, value=value))
    return config_keywords
