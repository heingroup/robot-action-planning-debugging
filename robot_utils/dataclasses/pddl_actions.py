import marshmallow
from pydantic.dataclasses import dataclass

from robot_utils.dataclasses.pddl_domain import domain


@dataclass
class Action:
    name: str
    function: str

    @marshmallow.validates("name")
    def validates(self, value):
        if not domain.has_action(value):
            raise ValueError(f"Invalid action {value}, does not exist")
