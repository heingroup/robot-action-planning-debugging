# from dataclasses import dataclass
from typing import Union, Optional

from pydantic.dataclasses import dataclass

from robot_utils.dataclasses.pddl_objects import Object, LLMObject
from robot_utils.dataclasses.pddl_predicates import OnlyInitialStatePredicate, ImpliedInitialStatePredicate, Predicate


# TODO: not great
# TODO: duplicated code


# TODO: check types of objects based on predicate
# TODO: validate for every predicate and every object pair there is an initial state
@dataclass
class PDDLProblemInstance:
    objects: list[Union[Object]]
    predicates: list[Union[Predicate, ImpliedInitialStatePredicate, OnlyInitialStatePredicate]]


@dataclass
class ChosenObjects:
    objects: list[LLMObject]


@dataclass
class LLMMessageFeedback:
    valid: bool
    feedback: str
