from unified_planning.io import PDDLReader

DOMAIN_SIMPLE = r"pddl/simple-liquid-domain.pddl"
DOMAIN_MOVE_PICKUP = r"pddl/move-pickup.pddl"


domain = PDDLReader().parse_problem(DOMAIN_SIMPLE, None)
