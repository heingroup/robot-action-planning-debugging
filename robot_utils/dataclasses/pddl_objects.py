from typing import Union

import marshmallow
from pydantic.dataclasses import dataclass

from robot_utils.dataclasses.pddl_domain import domain


# TODO figure out how to validate objects
@dataclass
class Object:
    name: str
    kind: str
    args: Union[dict[str, Union[str, int, float, bool, dict]], None]

    @marshmallow.validates('kind')
    def validates(self, value):
        if value not in [t.name for t in domain.user_types]:
            raise ValueError(f"Invalid object type {value}")


# TODO: remove duplication
@dataclass
class LocationObject:
    name: str
    edges: set[str]


@dataclass
class LLMObject:
    name: str
    kind: str

    @marshmallow.validates('kind')
    def validates(self, value):
        if value not in [t.name for t in domain.user_types]:
            raise ValueError(f"Invalid object type {value}")
