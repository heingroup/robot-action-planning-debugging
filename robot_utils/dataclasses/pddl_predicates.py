from typing import Union

import marshmallow
from marshmallow import validates_schema
from pydantic.dataclasses import dataclass

from robot_utils.dataclasses.pddl_objects import Object
from robot_utils.dataclasses.pddl_validation import check_predicate_existence, check_value_type

from robot_utils.dataclasses.pddl_domain import domain


@dataclass
class ChosenPredicate:
    predicate: str
    step: str

    @marshmallow.validates('predicate')
    def validates(self, value):
        if value not in [f.name for f in domain.fluents]:
            raise ValueError(f"Invalid predicate type {value}, does not exist")


@dataclass
class OnlyInitialStatePredicate:
    predicate: str
    objects: list[Union[Object, str]]
    initial_value: Union[int, bool]

    @validates_schema
    def validates(self, data, **kwargs):
        predicate = data["predicate"]
        initial_value = data["initial_value"]
        fluent_type = check_predicate_existence(predicate)
        check_value_type(predicate, fluent_type, initial_value, "initial")


@dataclass
class ImpliedInitialStatePredicate:
    predicate: str
    objects: list[Union[Object, str]]
    final_value: Union[int, bool]

    @validates_schema
    def validates(self, data, **kwargs):
        predicate = data["predicate"]
        final_value = data["final_value"]
        fluent_type = check_predicate_existence(predicate)
        check_value_type(predicate, fluent_type, final_value, "final")


@dataclass
class Predicate:
    predicate: str
    objects: list[Union[Object, str]]
    initial_value: Union[int, bool]
    final_value: Union[int, bool]

    @validates_schema
    def validates(self, data, **kwargs):
        predicate = data["predicate"]
        initial_value = data["initial_value"]
        final_value_type = data["final_value"]
        fluent = check_predicate_existence(predicate)

        if fluent.is_bool_type():
            if not (isinstance(initial_value, bool) and isinstance(final_value_type, bool)):
                raise marshmallow.ValidationError(
                    f"This predicate: {predicate} requires a boolean value for the initial and final values.")

        elif fluent.is_real_type() or fluent.is_int_type():
            if not (isinstance(initial_value, int) and isinstance(final_value_type, int)):
                raise marshmallow.ValidationError(
                    f"This predicate: {predicate} requires a numerical value for the initial and final values.")
        else:
            raise marshmallow.ValidationError(
                "Should not reach this branch when checking types of initial and final values.")


@dataclass
class ImpliedInitialStatePredicateStr:
    predicate: str
    objects: list[str]
    final_value: Union[int, bool]

    @validates_schema
    def validates(self, data, **kwargs):
        predicate = data["predicate"]
        objects = data["objects"]
        final_value = data["final_value"]
        if predicate not in [f.name for f in domain.fluents]:
            raise ValueError(f"invalid predicate type {predicate}, does not exist")
        fluent_type = check_predicate_existence(predicate)
        check_value_type(predicate, fluent_type, final_value, "final")
        objects_sig = domain.fluent(predicate).signature
        if len(objects_sig) != len(objects):
            raise marshmallow.ValidationError(
                f"For the predicate: {predicate}, expected {len(objects_sig)} objects, received {len(objects)} objects.")


@dataclass
class LLMPredicates:
    predicates: list[ImpliedInitialStatePredicateStr]
