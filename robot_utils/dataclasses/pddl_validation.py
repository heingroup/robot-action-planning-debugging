from __future__ import annotations

import builtins
from typing import Union

import marshmallow
from unified_planning.model import Type

from robot_utils.dataclasses.pddl_domain import domain


def check_predicate_existence(predicate: str) -> Type:
    # check that predicate is in the domain
    if predicate not in [f.name for f in domain.fluents]:
        raise marshmallow.ValidationError(f"{predicate} does not exist, choose one of the provided predicates.")
    fluent_type = domain.fluent(predicate).type
    return fluent_type


def check_value_type(predicate: str, fluent_type: Type, value: Union[int, bool, float], value_time: str):
    if fluent_type.is_bool_type():
        if not type(value) is bool:
            raise marshmallow.ValidationError(
                f"This predicate: {predicate} requires a boolean value for the {value_time} value.")
    elif fluent_type.is_real_type() or fluent_type.is_int_type():
        if not (type(value) is int or type(value) is float):
            raise marshmallow.ValidationError(
                f"This predicate: {predicate} requires an numerical value for the {value_time} value.")
    else:
        raise marshmallow.ValidationError(f"Should not each this branch when checking value type vs. fluent type.")


def check_type_and_father(obj_type: Type, x: str) -> bool:
    while obj_type.father:
        return obj_type.name == x or check_type_and_father(obj_type.father, x)
    return obj_type.name == x
