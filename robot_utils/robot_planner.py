"""
The main class for robot_utils.
"""

from __future__ import annotations

import copy
import json
import os
from typing import Union, Callable

import marshmallow_dataclass
import ollama
from unified_planning.engines import PlanGenerationResult
from unified_planning.io import PDDLReader, PDDLWriter
from unified_planning.model import Object, Fluent, Type, Variable, FNode
from unified_planning.plans import SequentialPlan, ActionInstance
from unified_planning.shortcuts import OneshotPlanner, Equals, Not

from dataclasses import dataclass
from llm_agent.llm import LLMAgent
from robot_utils.abstract_robot_classes.moveableconfig import MoveableConfig
from robot_utils.action_mapper.action_mapper import ActionMapper
from robot_utils.dataclasses.pddl_validation import check_type_and_father
from robot_utils.location import Location
from robot_utils.dataclasses.pddl_dataclasses import PDDLProblemInstance
from robot_utils.dataclasses.pddl_objects import LocationObject
from robot_utils.dataclasses.pddl_predicates import OnlyInitialStatePredicate, ImpliedInitialStatePredicate, Predicate


# TODO: add constraint only one moveable gripped at a time

@dataclass
class SafePath:
    start: Location
    end: Location
    path: list[Location]


class RobotActionGenerator:
    def __init__(self,
                 central_node: str,
                 domain_path: str,
                 user_input: str,
                 ip: str,
                 model: str,
                 critic: str,
                 moveable_sequence_type: type,
                 moveable_config_type: type,
                 object_mapping: dict[str: any],
                 action_mapping: dict[str: Callable],
                 moveable_configs: list[MoveableConfig],
                 problem_path=None,
                 debug: bool = False,
                 solver_type: str = "enhsp") -> None:

        if os.path.exists("dose_two_vial.py"):
            os.remove("dose_two_vial.py")

        self.debug = debug
        self.solver_type = solver_type
        self.reader = PDDLReader()
        if problem_path:
            self.pddl_file_problem = self.reader.parse_problem(domain_path, problem_path)
            self.plan = self.numeric_solve()

        # set up domain of robotic arm deck
        self.domain = self.reader.parse_problem(domain_path, None)

        # domain in PDDL form, constraints in JSON form, goal from the LLM
        self.problem = self.domain.clone()
        self.fluent_names = [f.name for f in self.problem.fluents]

        self.moveable_configs = moveable_configs
        pddlproblamclass = marshmallow_dataclass.class_schema(PDDLProblemInstance)()
        self.user_input_obj: PDDLProblemInstance = pddlproblamclass.load(json.loads(open(user_input, "r").read()))
        self.initialize_user_provided_inputs()

        # generate graph of safe paths
        self.locations = self.extract_edges()
        self.paths: Location = self.generate_paths(central_node)

        # llm related set up
        self.llm_agent = LLMAgent(model, critic, ip, self.problem)
        self.client = ollama.Client(host=ip)
        llm_predicates = self.llm_agent.prose_to_pddl(debug=debug)
        print(llm_predicates)
        self.ground_predicates(llm_predicates)

        # trajectory constraints set up
        # TODO: possibly LLM task
        self.set_up_trajectory_constraints()

        # solve problem
        self.plan: SequentialPlan = self.numeric_solve()
        self.safe_paths: dict[str, [SafePath]] = {}
        self.path_mutated_actions: list[ActionInstance] = self.find_viable_paths()

        # codegen
        self.autogen_code = ActionMapper(problem=self.problem,
                                         pddl_objects=self.user_input_obj.objects,
                                         plan=self.path_mutated_actions,
                                         object_mapping=object_mapping,
                                         debug=self.debug,
                                         moveable_configs=moveable_configs,
                                         action_mapping=action_mapping,
                                         moveable_sequence_type=moveable_sequence_type,
                                         moveable_config_type=moveable_config_type).generate_file()

        f = open("experiment.py", "x")
        f.write(self.autogen_code)
        f.close()

        if self.debug:
            w = PDDLWriter(self.problem)
            w.print_domain()
            w.print_problem()

    def generate_paths(self, central_node: str) -> Location:
        seen: dict[str, Location] = {}

        def generate_edges(node: Location) -> Location:
            edges = []
            for location in self.locations:
                if location.name == node.name:
                    edges = location.edges

            if len(edges) > 0 and node.name not in seen:
                seen[node.name] = node
                seen[node.name].nodes = [seen[n] if n in seen.keys() else generate_edges(Location(n)) for n in edges]

            return node

        return generate_edges(Location(central_node))

    def initialize_user_provided_inputs(self):
        objects = self.user_input_obj.objects
        predicates = self.user_input_obj.predicates

        for pddl_object in objects:
            self.problem.add_object(pddl_object.name, self.problem.user_type(pddl_object.kind))

        self.set_initial_and_final_fluent_states()
        self.ground_predicates(predicates)

    def ground_predicates(self, predicates: list[
        Union[ImpliedInitialStatePredicate, Predicate, OnlyInitialStatePredicate]]) -> None:

        def return_goal_predicate(predicate_to_ground: Fluent, ordered_args: list[Object], predicate) -> FNode:
            if predicate_to_ground.type.is_bool_type():
                return predicate_to_ground(*ordered_args) if predicate.final_value else \
                    Not(predicate_to_ground(*ordered_args))
            else:
                return Equals(predicate_to_ground(*ordered_args), predicate.final_value)

        for predicate in predicates:
            predicate_to_ground = self.problem.fluent(predicate.predicate)
            arg_types = [p._typename.name for p in predicate_to_ground.signature]
            pddl_objects = [self.problem.object(n) for n in predicate.objects] if isinstance(predicate.objects[0],
                                                                                             str) else \
                [self.problem.object(n.name) for n in predicate.objects]
            ordered_args = [obj for x in arg_types for obj in pddl_objects if check_type_and_father(obj.type, x)]

            predicate_type = type(predicate)
            if predicate_type == OnlyInitialStatePredicate:
                self.problem.set_initial_value(predicate_to_ground(*ordered_args), value=predicate.initial_value)
            else:
                goal = return_goal_predicate(predicate_to_ground, ordered_args, predicate)
                if predicate_type == ImpliedInitialStatePredicate:
                    if predicate_to_ground.type.is_bool_type():
                        self.problem.set_initial_value(predicate_to_ground(*ordered_args), value=False)
                    else:
                        self.problem.set_initial_value(predicate_to_ground(*ordered_args), value=0)
                else:
                    self.problem.set_initial_value(predicate_to_ground(*ordered_args), value=predicate.initial_value)
                self.problem.add_goal(goal)

    def numeric_solve(self) -> SequentialPlan:
        with OneshotPlanner(name=self.solver_type) as planner:
            result: PlanGenerationResult = planner.solve(self.problem)
            plan: SequentialPlan = result.plan
        if plan is not None:
            print("%s returned:" % planner.name)
            print(f"{plan}")
            return plan
        else:
            print("No plan found.")

    def set_initial_and_final_fluent_states(self):
        predicates: list[OnlyInitialStatePredicate] = []
        numerical_fluents = list(filter(lambda f_name: not self.problem.fluent(f_name).type.is_bool_type(),
                                        self.fluent_names))

        # fluents must all have their initial value set
        for fluent_name in numerical_fluents:
            predicate_to_ground: Fluent = self.problem.fluent(fluent_name)
            arg_types: list[Type] = [p.type for p in predicate_to_ground.signature]

            # TODO: generalise to more than 2 or less than 2 parameters
            parameter_1: Type = arg_types[0]
            parameter_2: Type = arg_types[1] if len(arg_types) > 1 else None

            def check_type(o, param_type: Type):
                obj: Object = self.problem.object(o.name)
                return (obj.type == param_type) or (o.type.father == param_type)

            objects_with_type_1: list[Object] = list(filter(lambda o: check_type(o, parameter_1),
                                                            self.problem.all_objects))

            objects_with_type_2: list[Object] = list(filter(lambda o: check_type(o, parameter_2),
                                                            self.problem.all_objects)) if parameter_2 is not None else []
            for obj_1 in objects_with_type_1:
                for obj_2 in objects_with_type_2:
                    predicates.append(OnlyInitialStatePredicate(
                        predicate=fluent_name,
                        objects=[obj_1.name, obj_2.name],
                        initial_value=0
                    ))

        self.ground_predicates(predicates)

    def get_nodes(self) -> list[Location]:
        seen = []

        def get_children_nodes(node: Location):
            children = []
            if node not in seen:
                seen.append(node)
                if len(node.nodes) > 0:
                    children += [get_children_nodes(n) for n in node.nodes]

        get_children_nodes(self.paths)

        return seen

    def find_safe_path(self, from_loc: str, to_loc: str) -> list[Location]:
        nodes = {n.name: n for n in self.get_nodes()}
        exploded = []
        start = nodes[from_loc]
        goal = nodes[to_loc]
        queue = [[start]]

        while queue:
            path = queue.pop(0)
            node = path[-1]

            if node not in exploded:
                neighbours = node.nodes

                for neighbour in neighbours:
                    new_path = list(path)
                    new_path.append(neighbour)
                    queue.append(new_path)

                    if neighbour.name == goal.name:
                        return new_path
                exploded.append(node)

    def find_viable_paths(self) -> list[ActionInstance]:
        actions_to_mutate: list[ActionInstance] = copy.deepcopy(self.plan.actions)
        shifted_index = 0

        for i, action_instance in enumerate(self.plan.actions):
            if "move" in action_instance.action.name:
                new_actions = []

                # TODO generalize this

                arm, location_from, location_to, *rest = action_instance.actual_parameters
                self.safe_paths[location_from.object().name] = []
                safe_path = self.find_safe_path(location_from.object().name, location_to.object().name)
                if len(safe_path) > 0:
                    # save the safe path incase it comes up again
                    self.safe_paths[location_from.object().name].append(safe_path)

                    # add to the actions
                    inner_shifted_index = -1
                    for j in range(len(safe_path) - 1):
                        from_loc: Location = safe_path[j]
                        to_loc: Location = safe_path[j + 1]
                        new_actions.append(ActionInstance(action=self.problem.action(action_instance.action.name),
                                                          params=(arm.object(),
                                                                  self.problem.object(from_loc.name),
                                                                  self.problem.object(to_loc.name),
                                                                  *[o.object() for o in rest])))
                        inner_shifted_index += 1
                    actions_to_mutate = actions_to_mutate[0:i + shifted_index] + new_actions + actions_to_mutate[
                                                                                               i + shifted_index + 1:]
                    shifted_index += inner_shifted_index
                else:
                    raise Exception(f"Currently no safe path between {location_from} and {location_to} can be found.")
        return actions_to_mutate

    def set_up_trajectory_constraints(self):
        # don't know how they work, maybe better to put in actions

        # TODO: is invarient after every action or only at the goal state

        # forall obj1 obj2 if obj1 at loc1, and obj2 at loc2, then loc1 != loc2
        # forall obj, obj must have location
        # TODO: put this in initial json or just assume this behaviour

        moveable = Variable("moveable", self.domain.user_type("moveable"))
        arm = Variable("arm", self.domain.user_type("arm"))

        # safe behaviour
        # arm ungripped
        # self.problem.add_state_invariant(
        #  Forall(Not(self.problem.fluent("moveable-gripped")(
        #     moveable,
        #    arm
        #  )), moveable, arm)

    # )

    # everything at allowed location ?
    # objects at home
    def extract_edges(self) -> list[LocationObject]:
        edges: dict[str, LocationObject] = {}
        for moveable_config in self.moveable_configs:
            safe_moves = moveable_config.safe_moves
            for safe_move in safe_moves:
                if safe_move.location_A not in edges.keys():
                    edges[safe_move.location_A] = LocationObject(name=safe_move.location_A, edges=set())
                if safe_move.location_B not in edges.keys():
                    edges[safe_move.location_B] = LocationObject(name=safe_move.location_B, edges=set())
                edges[safe_move.location_A].edges.add(safe_move.location_B)
                edges[safe_move.location_B].edges.add(safe_move.location_A)
        return list(edges.values())
