import ast
import inspect
from _ast import ImportFrom, alias, Module, Constant, Name, Assign, Call, Store, Load, keyword

from decks.kinova.chemicals import Solid
from decks.kinova.kinova_arm import KinovaArm
from decks.kinova.moving.hardcoded_moves import *
from decks.kinova.moving.hardcoded_pickup import liquid_handler_put_down, liquid_handler_pick_up
from decks.kinova.moving.kinova_movable_config import KinovaMoveableSequence, KinovaMoveableConfig
from robot_utils.action_mapper import ActionMapper

print(inspect.getmodule(KinovaArm).__file__)
lmaoo = inspect.getmodule(KinovaArm.pick_up)
obj_paths = ActionMapper.__class__.__module__
print(ActionMapper.__class__.__qualname__)
print(obj_paths)
print(obj_paths)
EUROPA = "europa"
EUROPA_BASE = "europa base"
REACTANT_BASE = "small reactants"
lmao = KinovaMoveableConfig(name=EUROPA,
                            safe_moves=[KinovaMoveableSequence(
                                location_A=EUROPA_BASE,
                                location_B=REACTANT_BASE,
                                sequence_from_A_to_B=home_to_liquid_handler_base,
                                sequence_from_B_to_A=liquid_handler_base_to_home)],

                            grip_open=0,
                            grip_close=0.29)
sequene = ast.parse(inspect.getsource(type(lmao.safe_moves[0])))

print(getattr(lmao, "safe_moves"))

lmaolmao = ast.parse(inspect.getsource(type(lmao)))

test = ast.parse("""
# a comment
handler = Handler(name=(1,2), config=KinovaMoveableConfig(name=EUROPA,
                              safe_moves=[KinovaMoveableSequence(
                                  location_A=EUROPA_BASE,
                                  location_B=REACTANT_BASE,
                                  sequence_from_A_to_B=liquid_to_small_reactants,
                                  sequence_from_B_to_A=liquid_from_small_reactants)],
                              pick_up=liquid_handler_pick_up,
                              put_down=liquid_handler_put_down,
                              grip_open=0,
                              grip_close=0.29))
handler.needle_down()
KinovaMoveableConfig(name=EUROPA,
                              safe_moves=[KinovaMoveableSequence(
                                  location_A=EUROPA_BASE,
                                  location_B=REACTANT_BASE,
                                  sequence_from_A_to_B=liquid_to_small_reactants,
                                  sequence_from_B_to_A=liquid_from_small_reactants)],
                              pick_up=liquid_handler_pick_up,
                              put_down=liquid_handler_put_down,
                              grip_open=0,
                              grip_close=0.29)
""")

SEQUENCES = None  # KinovaSequence(SEQUENCE_PATH)
ast_obj = Module(
    body=[
        ImportFrom(
            module='ur3.ur3_deck',
            names=[
                alias(name='move'),
                alias(name='draw_liquid'),
                alias(name='dispense_liquid'),
                alias(name='needle_up'),
                alias(name='Chemical'),
                alias(name='Handler'),
                alias(name='Container'),
            ],
            level=0),
        ImportFrom(
            module='hein_robots.universal_robots.ur3',
            names=[
                alias(name='Location'),
            ],
            level=0),
        Assign(
            lineno=0,
            targets=[Name(id='b', ctx=Store())],
            value=Constant(value=1)),
        Assign(
            lineno=0,
            targets=[Name(id='handler', ctx=Store())],
            value=Call(args=[], func=Name(id="Handler", ctx=Load()),
                       keywords=[keyword(arg="name", value=Constant("bruh")),
                                 keyword(arg="lmao", value=Constant(5))])),
    ],
    type_ignores=[])

# f = open("ast_test.py", "r+")
bruh = ast.parse("Handler('test')")
print(ast.unparse(ast_obj))
